<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache


/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'irk_wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '5m]eRxeGY(@Sf1^9s6PEm0why1l|TfI)<*|?k2elP]Le94yUq<{Z@@|=KWuH!^8H');
define('SECURE_AUTH_KEY',  'cDX M3;>c1=J,~0zeYI$WjR3RtZGRojP]&T66nTh0WeDi97`$-)hm<K,6nMUUB&m');
define('LOGGED_IN_KEY',    ' g1w<:W6Z,%ze`kz~?zw#Xgy.4t~dzqDlZ?Q`V+>BJ2a~VzrE8W3!H_>2ubxOfb8');
define('NONCE_KEY',        'c%XsC=|KVVA53%xFJ4&2b]*sufJO=s0[~LDD^w!%iX+IAkJEb)RCx*%BMt9y.Hk ');
define('AUTH_SALT',        'B=AYr&<Z=K&seqx3CNh=7Uel+Vfc2tw(nDv07n`)lSzlAiMk`~6,yK r/6aNA<nv');
define('SECURE_AUTH_SALT', '&82!&|E4L(iLFG9FN:M?AK_j3B!6#VZHz=)/|A~<d%2}50zH{fWA^_m]xdt4p^$>');
define('LOGGED_IN_SALT',   '#?+>V$yO~co*|ROs_3,VFkyV8j3ZJ;Q6erw<Kfi3,)NG3Y(l^qHvlstJQ|L5nY: ');
define('NONCE_SALT',       '~3U:$N^,EU*DC9S8Pqe%rXh_w13@1-Yq2Gd`O.mK)d}wx&1]d]p57gX-%LpQ]+t*');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('CONCATENATE_SCRIPTS', false);
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
