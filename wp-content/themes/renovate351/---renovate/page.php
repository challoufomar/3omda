<?php
get_header();
?>
<div class="theme-page padding-bottom-70">
	<div class="vc_row wpb_row vc_row-fluid gray full-width page-header vertical-align-table">
		<div class="vc_row wpb_row vc_inner vc_row-fluid full-width padding-top-bottom-50 vertical-align-cell">
			<div class="vc_row wpb_row vc_inner vc_row-fluid">
				<div class="page-header-left">
					<h1><?php the_title(); ?></h1>
                    <span class="bread-crumb">
					<?php if(function_exists('bcn_display'))
                    {
                        bcn_display();
                    }?>
						</span>
				</div>
				<div class="page-header-right">
					<!--div class="bread-crumb-container">
						<label><?php _e("You Are Here:", 'renovate'); ?></label>
						<ul class="bread-crumb">
							<li>
								<a href="<?php echo get_home_url(); ?>" title="<?php esc_attr_e('Home', 'renovate'); ?>">
									<?php _e('HOME', 'renovate'); ?>
								</a>
							</li>
							<li class="separator">
								&#47;
							</li>
							<li>
								<?php the_title(); ?>
							</li>
						</ul>
					</div-->
				</div>
			</div>
		</div>
	</div>
	
	<div class="clearfix">
	<div class=" col-md-9">
		<?php
		if(have_posts()) : while (have_posts()) : the_post();
			the_content();
		endwhile; endif;
		?>
	</div>
	<div class='right-form-devi col-md-3'>
	
	<?php echo do_shortcode(' [contact-form-7 id="1117" title=":frReservation:"]'); ?> 
</div>
<div class='soc-ico'>
	<?php echo do_shortcode(' [cn-social-icon alignment="left" display="vertical" attr_id="icon-soc" selected_icons="1,2,3"]'); ?> 

</div>
</div>
	<div class="clearfix">
</div>
</div>
<div class="site-container">
<?php
get_footer(); 
?>