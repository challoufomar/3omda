<?php
/*
Template Name: Default without breadcrumbs
*/
get_header();
?>
<div class="theme-page">
	<div class="clearfix">
		<?php
		if(have_posts()) : while (have_posts()) : the_post();
			the_content();
		endwhile; endif;
		?>
	</div>
</div>

<div class='soc-ico'>
	<?php echo do_shortcode(' [cn-social-icon alignment="left" display="vertical" attr_id="icon-soc" selected_icons="1,2,3"]'); ?> 

</div>
<?php
get_footer(); 
?>