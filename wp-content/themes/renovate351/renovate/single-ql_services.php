<?php
/*
Template Name: Service
*/
get_header();
?>
<div class="theme-page padding-bottom-70">
	<div class="vc_row wpb_row vc_row-fluid gray full-width page-header vertical-align-table">
		<div class="vc_row wpb_row vc_inner vc_row-fluid full-width padding-top-bottom-50 vertical-align-cell">
			<div class="vc_row wpb_row vc_inner vc_row-fluid">
				<div class="page-header-left">
					<h1><?php the_title(); ?></h1>
                    <span class="bread-crumb">
					<?php if(function_exists('bcn_display'))
                    {
                        bcn_display();
                    }?>
						</span>
				</div>
				<div class="page-header-right">
					<div class="bread-crumb-container">
						<label><?php _e("You Are Here:", 'renovate'); ?></label>
						<ul class="bread-crumb">
							<li>
								<a href="<?php echo get_home_url(); ?>" title="<?php esc_attr_e('Home', 'renovate'); ?>">
									<?php _e('HOME', 'renovate'); ?>
								</a>
							</li>
							<li class="separator">
								&#47;
							</li>
							<li>
								<?php the_title(); ?>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="clearfix   col-md-9 service-post">
		<?php
		/*get page with single post template set*/
		$post_template_page_array = get_pages(array(
			'post_type' => 'page',
			'post_status' => 'publish',
			//'number' => 1,
			'meta_key' => '_wp_page_template',
			'meta_value' => 'single-ql_services.php'
		));
		if(count($post_template_page_array))
		{
			$post_template_page = $post_template_page_array[0];
			if(count($post_template_page_array) && isset($post_template_page))
			{
				echo wpb_js_remove_wpautop(apply_filters('the_content', $post_template_page->post_content));
				global $post;
				$post = $post_template_page;
				setup_postdata($post);
			}
			else
				echo wpb_js_remove_wpautop(apply_filters('the_content', '[vc_row top_margin="none" el_class="margin-top-70"][vc_column type="" top_margin="none" width="1/4"][vc_wp_custommenu nav_menu="22" el_class="vertical-menu"][call_to_action_box title="COST CALCULATOR" text="" icon="wallet" button_label="REQUEST A QUOTE" button_url="#" top_margin="page-margin-top"]Use our form to estimate the initial cost of renovation or installation.[/call_to_action_box][box_header title="Download Brochures" type="h6" bottom_border="1" top_margin="page-margin-top"][vc_btn type="action" icon="arrow-circle-down" title="Download Brochure" url="#" top_margin="none" extraclass="margin-top-30"][vc_btn type="action" icon="arrow-circle-down" url="#" top_margin="none" extraclass="margin-top-10" title="Download Summary"][/vc_column][vc_column type="" top_margin="none" width="3/4"][single_service show_social_icons="1" show_twitter="1" show_facebook="1" show_linkedin="1" show_skype="1" show_googleplus="1" show_instagram="1" top_margin="none"][/vc_column][/vc_row]'));
		}
		else
		{
			if(function_exists("vc_map"))
				echo wpb_js_remove_wpautop(apply_filters('the_content', '[vc_row top_margin="none" el_class="margin-top-70"][vc_column type="" top_margin="none" width="1/4"][vc_wp_custommenu nav_menu="22" el_class="vertical-menu"][call_to_action_box title="COST CALCULATOR" text="" icon="wallet" button_label="REQUEST A QUOTE" button_url="#" top_margin="page-margin-top"]Use our form to estimate the initial cost of renovation or installation.[/call_to_action_box][box_header title="Download Brochures" type="h6" bottom_border="1" top_margin="page-margin-top"][vc_btn type="action" icon="arrow-circle-down" title="Download Brochure" url="#" top_margin="none" extraclass="margin-top-30"][vc_btn type="action" icon="arrow-circle-down" url="#" top_margin="none" extraclass="margin-top-10" title="Download Summary"][/vc_column][vc_column type="" top_margin="none" width="3/4"][single_service show_social_icons="1" show_twitter="1" show_facebook="1" show_linkedin="1" show_skype="1" show_googleplus="1" show_instagram="1" top_margin="none"][/vc_column][/vc_row]'));
		}
		?>

    </div>
    <div class='right-form-devi col-md-3'>

        <?php echo do_shortcode(' [contact-form-7 id="1117" title=":frReservation:"]'); ?>
    </div>

</div>

<div class="vc_row wpb_row vc_row-fluid">
	<div class="contact-div wpb_column vc_column_container vc_col-sm-12">
		<div class="wpb_wrapper">
			<div class="wpb_raw_code wpb_content_element wpb_raw_html">
				<div class="wpb_wrapper">
					<span class="popmake-contact-popup pum-trigger" style="cursor: pointer;">Contactez-nous</span>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
get_footer();
?>
<script type="text/javascript">
    var menuChassis=[
    '/services/chassis-pvc/',
    '/services/chassis-aluminium/',
    '/services/chassis-bois/',
    '/services/portes-d-entree/',
    '/services/portes-de-garage/',
    '/services/volets/',
    '/services/vitrages/'
    ]
    if(menuChassis.includes(window.location.pathname)){
        menu_id="menu-item-1238";//chassis
    }
    var menuFacades=[
    '/services/nettoyage/',
    '/services/restauration/',
    '/services/rejointoiement/',
    '/services/cimentage/',
    '/services/peinture-exterieure/',
    '/services/hydrofugation-protection/',
    '/services/isolation/'
    ]
    if(menuFacades.includes(window.location.pathname)){
        menu_id="menu-item-1279";//Façade
    }
    var menuRenovation=[
    '/services/toiture/',
    '/services/peinture/',
    '/services/plafonnage/',
    '/services/parquet-et-carrelage/',
    '/services/plomberie-et-chauffage/',
    '/services/electricite/',
    '/services/maconnerie/'
    ]
    if(menuRenovation.includes(window.location.pathname)){
        menu_id="menu-item-1335";//Renovation
    }
    var menuConstruction=[
    '/services/maison-cle-sur-porte/',
    '/services/gros-oeuvre/'
    ]
    if(menuConstruction.includes(window.location.pathname)){
        menu_id="menu-item-1263";//Toitrue
    }
    var menuToiture =[
    '/services/couverture/',
    '/services/toiture-isolation/',
    '/services/demoussage/',
    '/services/charpente/',
    '/services/zinguerie/',
    '/services/plateforme/'
    ]
    var menuToitureTitles =[
        'Couverture',
        'Isolation',
        'Démoussage',
        'Charpente',
        'Zinguerie',
        'Plateforme'
    ]
    if(menuToiture.includes(window.location.pathname)){
        menu_id="menu-item-1334";//Toitrue
    }


    if(menu_id=="menu-item-1334") {
        var x = document.getElementsByClassName("wpb_wrapper");
        x[0].innerHTML="" ;

        for (var i = 0; i < menuToiture.length; ++i) {

            if(menuToiture[i] == window.location.pathname){
                if(i==0){
                    x[0].innerHTML +='<li  class="service-link"><a style="background: #af100c;color: #F5F5F5;" >'+menuToitureTitles[i]+'</a></li>';

                }else{

                    x[0].innerHTML +='<li  class="service-link"><a style="background: #af100c;color: #F5F5F5;" >'+menuToitureTitles[i]+'</a></li>';
                }
            }
            console.log(i);
            if(menuToiture[i] != window.location.pathname){
                if(i==0){
                    x[0].innerHTML +='<li class="service-link"><a href="'+menuToiture[i]+'">'+menuToitureTitles[i]+'</a></li>';
                }else{
                    x[0].innerHTML +='<li class="service-link"><a href="'+menuToiture[i]+'">'+menuToitureTitles[i]+'</a></li>';
                }
            }

        }
    }
    else {
        var lis = document.getElementById(menu_id).getElementsByTagName("li");
        var x = document.getElementsByClassName("wpb_wrapper");

        console.log(window.location.pathname);
        for (var i = 0; i < lis.length; ++i) {
            console.log(lis[i].getElementsByTagName("a")[0].getAttribute("href")+'/');
            if(lis[i].getElementsByTagName("a")[0].getAttribute("href")!= window.location.pathname && lis[i].getElementsByTagName("a")[0].getAttribute("href")+'/' != window.location.pathname ){
                if(i==0){
                    x[0].innerHTML ='<li class="service-link"><a href="'+lis[i].getElementsByTagName("a")[0].href+'">'+lis[i].textContent+'</a></li>';
                }else{
                    x[0].innerHTML +='<li class="service-link"><a href="'+lis[i].getElementsByTagName("a")[0].href+'">'+lis[i].textContent+'</a></li>';
                }
            }
            else{
                if(i==0){
                    x[0].innerHTML ='<li class="service-link"><a style="background: #af100c;color: #F5F5F5;">'+lis[i].textContent+'</a></li>';
                }else{
                    x[0].innerHTML +='<li class="service-link"><a style="background: #af100c;color: #F5F5F5;" >'+lis[i].textContent+'</a></li>';
                }
            }

        }
    }

</script>