<?php
/*
Template Name: Default without breadcrumbs
*/
get_header();
?>
<div class="theme-page">
	<div class="clearfix">
		<?php
		if(have_posts()) : while (have_posts()) : the_post();
			the_content();
		endwhile; endif;
		?>
	</div>
</div>

<div class='soc-ico'>
	<?php echo do_shortcode(' [cn-social-icon alignment="left" display="vertical" attr_id="icon-soc" selected_icons="1,2,3"]'); ?> 

</div>
<?php
get_footer(); 
?>
<div class="vc_row wpb_row vc_row-fluid">
	<div class="contact-div wpb_column vc_column_container vc_col-sm-12">
		<div class="wpb_wrapper">
			<div class="wpb_raw_code wpb_content_element wpb_raw_html">
				<div class="wpb_wrapper">
					<span class="popmake-contact-popup pum-trigger" style="cursor: pointer;">Contactez-nous</span>
				</div>
			</div>
		</div>
	</div>
</div>