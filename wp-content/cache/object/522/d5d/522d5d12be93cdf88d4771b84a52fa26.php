"��[<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:983;s:11:"post_author";s:1:"5";s:9:"post_date";s:19:"2018-07-18 17:39:29";s:13:"post_date_gmt";s:19:"2018-07-18 15:39:29";s:12:"post_content";s:16663:"[:fr][vc_row][vc_column][vc_column_text]
<h1></h1>
 
<h1 style="text-align: center;">Les primes</h1>
 
<p class="user-content">Comme vous en avez certainement entendu parler, il existe de nombreuses possibilités de demandes de primes lorsque des travaux de rénovation d'une maison, d'un appartement ou d'un immeuble sont entrepris.</p>
A titre d'exemples, vous trouverez ci-dessous une liste non exhaustive de primes pouvant être demandées lors de travaux de rénovation dans la Région de Bruxelles-Capitale :

 

[/vc_column_text][/vc_column][/vc_row][vc_row][vc_column][vc_accordion][vc_accordion_tab title="Primes à la rénovation du logement" tab_id="1532007816-1-55"][vc_nested_tabs][vc_nested_tab title="Primes à la rénovation du logement" tab_id="1532007816-1-15"][vc_column_text]Afin de pouvoir bénéficier de la prime à la rénovation de votre logement, un certain nombres de conditions préalables doivent être remplies :

 
<h6 style="text-align: left;">•  être le propriétaire occupant du logement qu’il occupe personnellement, en tant que personne physique, et s'engager à ne pas vendre, ni louer, ce logement pendant 5 ans.</h6>
<h6 style="text-align: left;">•  être le propriétaire bailleur, en tant que personne morale ou physique, et confier la gestion locative du logement rénové à une Agence Immobilière Sociale pendant 9 ans minimum, ainsi que s'engager à ne pas vendre ce logement avant 10 ans</h6>
<h6 style="text-align: left;">•  le logement doit avoir été construit depuis plus de 30 ans, mais il pouvait être destiné à une autre affectation avant les travaux.</h6>
<h6 style="text-align: left;">•  quels que soient les revenus du propriétaire occupant pour autant que le logement se situe dans l'Espace de Développement Renforcé du Logement et de la Rénovation</h6>
<h6 style="text-align: left;">•  avoir des revenus inférieurs à 67 050,72€ en tant que propriétaire occupant d’un logement situé hors de l'Espace de Développement Renforcé du Logement et de la Rénovation</h6>
<h6 style="text-align: left;">•  la demande de primes doit impérativement être introduite avant les travaux auprès du Ministère de la Région de Bruxelles-Capitale Direction du Logement sur le formulaire adéquat, avec quelques documents administratifs ainsi que le devis des travaux</h6>
<h6 style="text-align: left;">•  attendre d'avoir reçu l'accusé de réception de la demande de primes émanant du Ministère de la Région de Bruxelles-Capitale. Dans l'accusé de réception, les renseignements suivants vous serons communiqués: les travaux acceptés, l'estimation du montant de la prime, l'autorisation de débuter les travaux.</h6>
<h6 style="text-align: left;">•  à partir de la date de l'accusé de réception, il vous est accordé 2 ans pour réaliser les travaux et transmettre les factures</h6>
<h6 style="text-align: left;">•  faire réaliser les travaux par un entrepreneur enregistré tel que l'entreprise de construction et de rénovation <strong>IRK.</strong></h6>
[/vc_column_text][/vc_nested_tab][vc_nested_tab title="Obtenir la prime à la rénovation du logement à bruxelles" tab_id="1532007816-2-81"][vc_column_text]
<p style="text-align: left;">Le calcul de la prime à la rénovation se fait sur base d'un pourcentage du montant des travaux acceptés. Ce pourcentage variera suivant que vous soyez propriétaire bailleur ou propriétaire occupant. Si vous êtes propriétaire occupant, il sera également pris en compte des revenus du ménage et du périmètre dans lequel se trouve le bien.</p>

<div></div>
<p style="text-align: left;">Vous trouverez ci-dessous une liste non exhaustive des travaux pouvant être subsidiés par les primes à la rénovation du logement :</p>
 
<h6 style="text-align: left;">•  travaux de stabilité tels que: fondations, poutres, colonnes, hourdis, dalle de béton, maçonnerie portante, ouverture et fermeture de baies dans des murs porteurs, gîtage en bois, ...</h6>
<h6 style="text-align: left;">•  travaux de toiture tels que: étanchéité de toitures plates ou à versants, démolition de souche de cheminée, charpentes, corniches, descentes d'eau pluviale, lucarnes, fenêtre de toiture, ...</h6>
<h6 style="text-align: left;">• travaux de traitement contre l'humidité</h6>
<h6 style="text-align: left;">•  travaux de ventilation mécanique individuel, simple flux ou double flux</h6>
<h6 style="text-align: left;">•  travaux d'installation électrique tels que: remplacement de l'installation électrique vétuste</h6>
<h6 style="text-align: left;">•  travaux d'installation de gaz tels que: remplacement des conduites de gaz vétustes</h6>
<h6 style="text-align: left;">•  travaux d'isolation thermique tels que: isolation de toitures, isolation de murs, isolation de plancher, ...</h6>
<h6 style="text-align: left;">•  travaux d'isolation acoustique tels que: isolation acoustique de murs et de planchers entre deux logements</h6>
<h6 style="text-align: left;">•  travaux de bardage pour murs extérieurs</h6>
<h6 style="text-align: left;">•  travaux d'enduits pour murs extérieurs</h6>
<h6 style="text-align: left;">•  travaux de menuiseries extérieures tels que: réparation de portes d'entrée et de châssis, pose de double vitrage dans des châssis existants, pose de châssis neufs en bois</h6>
<h6 style="text-align: left;">•  travaux d'installation de chauffage tels que: placement d'une chaudière à gaz à condensation et installation des radiateurs</h6>
<h6 style="text-align: left;">•  travaux d'instalations sanitaires tels que : placement des arrivées d'eau et décharges d'eaux usées, pose de WC, de lavabos, de douches, de baignoires, d'éviers, de robinets, ...</h6>
<h6 style="text-align: left;">•  travaux d'égoutage</h6>
<h6 style="text-align: left;">•  travaux de menuiseries intérieures tels que: la pose d'escaliers</h6>
[/vc_column_text][/vc_nested_tab][/vc_nested_tabs][/vc_accordion_tab][vc_accordion_tab title="Primes à l'énergie" tab_id="1532007816-2-88"][vc_nested_tabs][vc_nested_tab title="Primes à l'énergie" tab_id="1532008868-1-26"][vc_column_text]
<p style="text-align: left;">Contrairement aux primes à la rénovation de l'habitat, certaines primes à l'énergie peuvent également être octroyées aux constructions neuves, d'autres primes à l'énergie ne sont octroyées qu'aux bâtiments de plus de 10 ans.</p>
<p style="text-align: left;">Les demandes de primes à l'énergie doivent être intoduites, après les travaux, auprès de Bruxelles Environnement dans un délai maximum de 4 mois après l'établissement de la facture finale des travaux ...</p>
[/vc_column_text][/vc_nested_tab][vc_nested_tab title="Obtenir le prime à l'énergie en région bruxelloise" tab_id="1532008868-2-62"][vc_column_text]
<p style="text-align: left;">Les primes à l'énergie étant liées à des conditions techniques en terme de performances énergétiques à atteindre, il convient de remplir un formulaire de demande par type de travaux.</p>
<p style="text-align: left;">Le montant alloué par les primes dépend des revenus du ménage du demandeur, de l'âge du demandeur, du nombre de personnes à charge du demandeur, du périmètre (Espace de Développement Renforcé du Logement et de la Rénovation) dans lequel se situe le bâtiment.</p>
<p style="text-align: left;">Chaque année, un budget est alloué à Bruxelles Environnement pour l'octroi des primes à l'énergie. Après épuisement du budget alloué, plus aucune prime à l'énergie ne sera accordée pour l'année en cours.</p>
<p style="text-align: left;">Vous trouverez ci-dessous une liste non exhaustive des travaux pouvant donnés droit à des primes à l'énergie :</p>
<p style="text-align: left;">Isolation et ventilation :</p>

<h6 style="text-align: left;">- Construction passive</h6>
<h6 style="text-align: left;">- Rénovation basse énergie</h6>
<h6 style="text-align: left;">- Rénovation très basse énergie</h6>
<h6 style="text-align: left;">- Rénovation passive</h6>
<h6 style="text-align: left;">- Isolation de toiture</h6>
<h6 style="text-align: left;">- Isolation des murs</h6>
<h6 style="text-align: left;">- Isolation du sol</h6>
<h6 style="text-align: left;">- Vitrage superisolant</h6>
<h6 style="text-align: left;">- Toiture verte</h6>
<h6 style="text-align: left;">- Protection solaire extérieure</h6>
<h6 style="text-align: left;">- Ventilation mécanique performante</h6>
<p style="text-align: left;">Chauffage performant</p>

<h6 style="text-align: left;">- Chaudière à condensation</h6>
<h6 style="text-align: left;">- Chauffe-eau instantané au gaz individuel</h6>
<h6 style="text-align: left;">- Régulation thermique</h6>
<h6 style="text-align: left;">- Pompe à chaleur</h6>
<p style="text-align: left;">Energies renouvelables</p>

<h6 style="text-align: left;">- Chauffe-eau solaire</h6>
<h6 style="text-align: left;">- Système photovoltaïque</h6>
[/vc_column_text][/vc_nested_tab][/vc_nested_tabs][/vc_accordion_tab][vc_accordion_tab title="Prime à l'embellissement de la façade"][vc_nested_tabs][vc_nested_tab title="Prime à l'embellissement de la façade" tab_id="1532010868-1-41"][vc_column_text]
<p style="text-align: left;">Afin de pouvoir bénéficier de la prime à l'embellissement des façades, un certain nombres de conditions préalables doivent être remplies :</p>
<p style="text-align: left;">• les immeubles doivent :</p>

<h6 style="text-align: left;">- dater de plus de 25 ans</h6>
<h6 style="text-align: left;">- être mitoyens sur au moins un de leur côté</h6>
<h6 style="text-align: left;">- avoir leur façade à front de rue ou en recul de 8 mètres maximum</h6>
<h6 style="text-align: left;">- être, pour au moins leur deux tiers, affectés au logement</h6>
<h6 style="text-align: left;">- ne pas avoir bénéficié d’une prime à l’embellissement des façades depuis au moins 20 ans</h6>
<p style="text-align: left;">• les bénéficiaires peuvent être, quelques soit leur revenu :</p>

<h6 style="text-align: left;">- le propriétaire, en tant que personne physique ou morale</h6>
<h6 style="text-align: left;">- le propriétaire, en tant que personne physique ou morale</h6>
<h6 style="text-align: left;">- une association de copropriétaires</h6>
<h6 style="text-align: left;">- le locataire avec un bail emphytéotique</h6>
<h6 style="text-align: left;">- le commerçant, domicilié en Région de Bruxelles-Capitale, avec un bail commercial n’arrivant pas à terme avant 6 ans</h6>
<h6 style="text-align: left;">- une asbl agréée oeuvrant à la rénovation de logement ou une AIS -Agence Immobilière Sociale</h6>
<p style="text-align: left;">• les bénéficiaires s'engagent à maintenir l’affectation en logement pour une durée minimale de 5 ans</p>
<p style="text-align: left;">• la demande de primes doit impérativement être introduite avant les travaux auprès du Ministère de la Région de Bruxelles-Capitale Direction du Logement sur le formulaire adéquat, avec quelques documents administratifs ainsi que le devis des travaux</p>
<p style="text-align: left;">• attendre d'avoir reçu l'accusé de réception de la demande de primes émanant du Ministère de la Région de Bruxelles-Capitale. Dans l'accusé de réception, les renseignements suivants vous serons communiqués: les travaux acceptés, l'estimation du montant de la prime.</p>
<p style="text-align: left;">• attendre la visite de l’enquêteur de l’Administration régionale avant d'entamer les travaux .</p>
<p style="text-align: left;">• à partir de la date d'accord de la Région de Bruxelles-Capitale, il vous est accordé 2 ans pour réaliser les travaux et transmettre les factures</p>
<p style="text-align: left;">• faire réaliser les travaux par un entrepreneur enregistré tel que l'entreprise de construction et de rénovation <strong>IRK.</strong></p>
[/vc_column_text][/vc_nested_tab][vc_nested_tab title="Obtenir la prime à l'embellissement de façade en région Bruxelloise" tab_id="1532010868-2-38"][vc_column_text]
<div style="text-align: left;">Le calcul de la prime à l'embellissement des façades se fait sur base d'un pourcentage du montant des travaux acceptés, du périmètre dans lequel se trouve le bien, ... .</div>
<p style="text-align: left;">Vous trouverez ci-dessous une liste non exhaustive des travaux pouvant être subsidiés par la prime à l'embellissement des façades:</p>
<p style="text-align: left;">• le traitement du parement de la façade:</p>

<h6 style="text-align: left;">- nettoyage des façades non peintes</h6>
<h6 style="text-align: left;">- remise en peinture des façades peintes</h6>
<h6 style="text-align: left;">- pose d’un hydrofuge</h6>
<h6 style="text-align: left;">- pose d’un anti-graffit</h6>
<p style="text-align: left;">• l'entretien et la mise en peinture des éléments de façade tels que: châssis de fenêtre, portes, tout élément en bois ou métallique (balcon, corniche,…)</p>
<p style="text-align: left;">• la réparation d'éléments de façades tels que: moulures des enduits, balcons, loggias, ...</p>
[/vc_column_text][/vc_nested_tab][/vc_nested_tabs][/vc_accordion_tab][vc_accordion_tab title="Primes et aides communales"][vc_column_text]
<h2 style="text-align: center;">Primes et aides communales</h2>
<p style="text-align: left;">Afin de favoriser certains types de travaux pour des immeubles situés sur leur territoire, certaines communes de la Région de Bruxelles-Capitale octroient des primes complémentaires aux primes régionales et/ou aux réductions d'impôt.
Les modifications de ces primes et de leurs conditions d'octroi étant nombreuses, et variables d'une commune à l'autre, nous vous invitons à vous informer auprès du service de l'urbanisme de votre commune.</p>
[/vc_column_text][/vc_accordion_tab][vc_accordion_tab title="Réduction d'impôt"][vc_nested_tabs][vc_nested_tab title="Réductions d'impôt" tab_id="1532010868-1-41"][vc_column_text]
<p style="text-align: left;">Dans le carde travaux de rénovation d'une habitation, il existe les possibilités suivantes d'obtenir une réduction d'impôt auprès du SPF Finances par l'intermédiaire de la déclaration d'impôt:</p>
[/vc_column_text][/vc_nested_tab][vc_nested_tab title="Réductions d'impôt pour des travaux d'isolation de toitures" tab_id="1532012425038-1-2"][vc_column_text]
<h6 style="text-align: left;">- concernent les habitations de plus de 5 ans</h6>
<h6 style="text-align: left;">- équivalentes à 30% du montant des travaux d'isolation de toitures, avec un maximum plafonné par habitation</h6>
<h6 style="text-align: left;">- pour autant que certaines conditions techniques soient respectées telles que la résistance thermique R de l'isolant soit supérieure ou égale à 2,5m²K/W</h6>
<h6 style="text-align: left;">- pour autant que la facture de l'entrepreneur reprenne certaines indications telles que: l'adresse de l'habitation où s'effectuent les travaux, la ventilation du coût des travaux en fonction de la nature de ceux-ci, la mention de la formule adéquate sur la facture ou en annexe de celle-ci</h6>
[/vc_column_text][/vc_nested_tab][vc_nested_tab title="Réductions d'impôt pour travaux de sécurisation contre le vol et l'incendie" tab_id="1532012592750-2-5"][vc_column_text]
<p style="text-align: left;">• concernent les travaux suivants:</p>

<h6 style="text-align: left;">- éléments de façades retardateurs d'intrusion</h6>
<h6 style="text-align: left;">- vitrages</h6>
<h6 style="text-align: left;">- systèmes de sécurisation</h6>
<h6 style="text-align: left;">- portes blindées</h6>
<h6 style="text-align: left;">- portes résistantes au feu</h6>
<h6 style="text-align: left;">- extincteurs</h6>
<p style="text-align: left;">• équivalentes à 30% du montant des travaux de sécurisation, avec un maximum plafonné par habitation</p>
<p style="text-align: left;">• pour autant que certaines conditions techniques</p>
<p style="text-align: left;">• pour autant que la facture de l'entrepreneur reprenne certaines indications telles que: l'adresse de l'habitation où s'effectuent les travaux, la ventilation du coût des travaux en fonction de la nature de ceux-ci, la mention de la formule adéquate sur la facture ou en annexe de celle-ci.</p>
[/vc_column_text][/vc_nested_tab][/vc_nested_tabs][/vc_accordion_tab][/vc_accordion][/vc_column][/vc_row][:]";s:10:"post_title";s:25:"[:fr]Primes & Subsides[:]";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:15:"zones-dactivite";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-08-27 15:29:36";s:17:"post_modified_gmt";s:19:"2018-08-27 13:29:36";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:38:"http://it-selexion.ma/irk/?page_id=983";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}