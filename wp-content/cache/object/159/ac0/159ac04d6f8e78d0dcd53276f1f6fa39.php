"��[<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:137;s:11:"post_author";s:1:"5";s:9:"post_date";s:19:"2015-05-20 11:09:20";s:13:"post_date_gmt";s:19:"2015-05-20 11:09:20";s:12:"post_content";s:3796:"[:fr][vc_row el_class="padding-top-70 padding-bottom-66"][vc_column][vc_row_inner top_margin="page-margin-top"][vc_column_inner][vc_column_text]
<h4 style="text-align: center;">Nous proposons nos services aussi bien aux particuliers, aux entreprises qu’aux institutions et ce, à travers toute la Belgique.</h4>
[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row][vc_row el_class="padding-bottom-66"][vc_column][ql_services order_by="menu_order" headers="1" headers_links="1" headers_border="1" show_excerpt="1" show_featured_image="1"][/vc_column][/vc_row][vc_row el_class="padding-bottom-66"][vc_column][vc_row_inner top_margin="page-margin-top"][vc_column_inner][vc_column_text]
<h4>Nous pouvons décliner nos services en 3 catégories :</h4>
[/vc_column_text][/vc_column_inner][/vc_row_inner][/vc_column][/vc_row][vc_row top_margin="page-margin-top"][vc_column][vc_nested_accordion][vc_nested_accordion_tab title="Construction" tab_id="1531930694-1-96"][vc_column_text] IRK Group dispose du savoir-faire pour prendre en charge la construction d’une maison ou d’un immeuble. Lorsque nous construisons des maisons, nous voulons que vous vous y sentiez bien … et pour longtemps. C’est la raison pour laquelle nous mettons un point d’honneur à respecter vos souhaits et vos attentes tout en étant transparent et objectif quant à la faisabilité de ceux-ci. Etant donné que chaque maison est unique, il vous reviendra de déterminer le type de livraison que vous souhaitez en fonction du niveau d’implication que vous souhaitez mettre à
votre projet : clé-sur-porte pour ne vous soucier de rien ou gros-oeuvre fermé pour pouvoir gérer vous-même les finitions. Plus qu’un prestataire, nous vous conseillerons depuis le premier entretien jusqu’à la livraison. Si vous envisagez construire, contactez-nous dès aujourd'hui pour planifier votre consultation.

[/vc_column_text][/vc_nested_accordion_tab][vc_nested_accordion_tab title="Rénovation" tab_id="1531930694-2-55"][vc_column_text]Choisir de remodeler votre logement est un réel investissement. Une rénovation réalisée dans les règles de l’art améliorera non seulement la qualité et la fonctionnalité de votre logement mais elle augmentera la valeur de celui-ci. Nos professionnels transformeront votre logement actuel en un espace de vie que votre famille aimera et dont elle sera extrêmement fière. Grâce à notre expertise dans l'optimisation de votre espace, nous pourrons très vraisemblablement éliminer le besoin de mètres carrés supplémentaires. Que vous souhaitiez rénover plusieurs pièces ou franchir le pas avec une rénovation complète, nos spécialistes vous proposeront des solutions efficaces et innovantes qui feront de votre vision une réalité. Si vous envisagez rénover, contactez-nous dès aujourd'hui pour planifier votre consultation.

[/vc_column_text][/vc_nested_accordion_tab][vc_nested_accordion_tab title="Travaux divers"][vc_column_text]IRK dispose d’une équipe importante composée de professionnels hautement qualifiés capables de vous conseiller et de prendre en charge tout type de projet :

&nbsp;
<h5>- Maçonnerie</h5>
<h5>- Structure en béton, en acier ou en bois</h5>
<h5>- Revêtements de façade</h5>
<h5>- Charpente</h5>
<h5>- Toiture</h5>
<h5>- Etanchéité</h5>
<h5>- Isolation thermique et acoustique</h5>
<h5>- Menuiseries extérieures</h5>
<h5>- Installations électriques, sanitaires et de chauffage</h5>
<h5>- Plafonnage</h5>
<h5>- Cloisons intérieures</h5>
<h5>- Faux plafonds</h5>
<h5>- Chapes</h5>
<h5>- Revêtements de sols</h5>
<h5>- Menuiseries intérieures</h5>
<h5>- Décoration - Peinture</h5>
[/vc_column_text][/vc_nested_accordion_tab][/vc_nested_accordion][/vc_column][/vc_row][:]";s:10:"post_title";s:25:"[:fr]Que faisons-nous?[:]";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:4:"open";s:11:"ping_status";s:4:"open";s:13:"post_password";s:0:"";s:9:"post_name";s:8:"services";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-08-01 10:45:54";s:17:"post_modified_gmt";s:19:"2018-08-01 08:45:54";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:48:"http://localhost/wordpress/renovate/?page_id=137";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}